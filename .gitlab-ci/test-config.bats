#!/usr/bin/env bats

setup() {
    # get the containing directory of this file
    # use $BATS_TEST_FILENAME instead of ${BASH_SOURCE[0]} or $0,
    # as those will point to the bats executable's location or the preprocessed file respectively
    DIR="$( cd "$( dirname "$BATS_TEST_FILENAME" )" >/dev/null 2>&1 && pwd )"
    StevenBlack_Hosts="https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts"
    Coinblock_Hosts="https://zerodot1.gitlab.io/CoinBlockerLists/hosts_browser"
}

@test "Generate DNS Zone file from StevenBlack's Hosts file" {
    "$DIR"/../gen-unbound-hosts.py -u -d "$StevenBlack_Hosts"
    mv unbound-blocklist.conf "$DIR/../blocklist-unbound-stevenblack.conf"
    mv dnsmasq-blocklist.conf "$DIR/../blocklist-dnsmasq-stevenblack.conf"
}

@test "Generate DNS Zone files from multiple sources" {
    "$DIR"/../gen-unbound-hosts.py -s "newstats.txt" -u -d "$StevenBlack_Hosts" $(wget -O- -q https://v.firebog.net/hosts/csv.txt | grep -e "tracking" -e "advertising" | grep "tick" | cut -d, -f5 | xargs) https://raw.githubusercontent.com/unknownFalleN/xiaomi-dns-blocklist/master/xiaomi_dns_block.lst https://gist.githubusercontent.com/BBcan177/2a9fc2548c3c5a5e2dc86e580b5795d2/raw/2f5c90ffb3bd02199ace1b16a0bd9f53b29f0879/EasyList_DE
    mv unbound-blocklist.conf "$DIR/../blocklist-unbound-consolidated.conf"
    mv dnsmasq-blocklist.conf "$DIR/../blocklist-dnsmasq-consolidated.conf"
}

@test "[Unbound] Test various generated configs are valid" {
    unbound-checkconf "$DIR/../blocklist-unbound-stevenblack.conf"
    unbound-checkconf "$DIR/../blocklist-unbound-consolidated.conf"
}

@test "[Dnsmasq] Test various generated configs are valid" {
    dnsmasq --test -C "$DIR/../blocklist-dnsmasq-stevenblack.conf"
    dnsmasq --test -C "$DIR/../blocklist-dnsmasq-consolidated.conf"
}
