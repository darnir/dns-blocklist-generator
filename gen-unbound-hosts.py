#!/usr/bin/env python3

# Generate DNS Zone Blocklists for use with unbound(8)
#
# Unbound supports wildcard entries. This script filters out subdomains of
# pre-existing domains in the list. As of this writing, when using the
# StevenBlack hosts file as input, this script reduces the 103110 hosts entries
# to 65010 unbound local zones.

# Usage: ./gen-unbound-hosts.py [hosts-files...]
#
# hosts-file can be either a local file or a URL. It will aggregate the hosts
# from all input files and create one final deduplicated output DNS Zone file
# for use with unbound.

# Authors: Darshit Shah <git@darnir.net>
#          Savyasachee Jha <genghizkhan91@hawkradius.com>
#
# License: GPLv3+


import argparse
import urllib.request

from pathlib import Path
from tld import is_tld


# Global variables
LOCALTARGETS = {"0.0.0.0", "127.0.0.1", "::1"}
LOCALHOSTS = {
    "localhost",
    "localhost.localdomain",
    "local",
    "broadcasthost",
    "ip6-localhost",
    "ip6-mcastprefix",
    "ip6-allnodes",
    "ip6-allrouters",
    "ip6-allhosts",
}


def get_hosts_file(url):
    return urllib.request.urlopen(url)


def add_hosts(hosts_file):
    host_count = 0
    errored: bool = False
    for line in hosts_file:
        line = line.decode("UTF-8").strip().split("#", 1)[0].strip(' .').lower()
        if not line:
            continue
        num_tokens = len(line.split())
        if num_tokens == 1:
            # If there is only one token, then assume there is no target
            host = line
            target = "0.0.0.0"
        elif num_tokens > 2:
            # This is an error in the upstream hosts file.
            # Log a warning and continue
            print(f"  => WARNING: {line}")
            errored = True
            continue
        else:
            target, host = line.split()

        if target in LOCALTARGETS and host not in (LOCALHOSTS or LOCALTARGETS):
            host_count += 1
            host_set.add(host)
    return host_count, host_set, errored


def add_to_blacklist(host, blacklist, created_whitelist):
    for j in range(host.count("."), 0, -1):
        if host.split(".", j)[-1] in blacklist:
            return False
    if is_tld(host):
        return False
    if host in created_whitelist:
        return False
    return True

def gen_whitelist_set(whitelist_set):
    created_whitelist = set()
    for host in sorted(whitelist_set, key = len):
        for j in range(host.count("."), -1, -1):
            to_be_added = host.split(".", j)[-1]
            if not is_tld(to_be_added):
                created_whitelist.add(to_be_added)
    return created_whitelist


def gen_blocklist(input_hosts, created_whitelist):
    blocklist = set()
    for host in sorted(input_hosts, key=len):
        if add_to_blacklist(host, blocklist, created_whitelist):
            blocklist.add(host)
    return blocklist


def write_unbound_blocklist_file(blist, fname):
    with open(fname, "w") as f:
        f.write("server:\n")
        for e in sorted(blist, key=len):
            f.write('local-zone: "' + e + '" always_nxdomain\n')

def write_dnsmasq_blocklist_file(blist, fname):
    with open(fname, "w") as f:
        for e in sorted(blist, key=len):
            f.write('server=/' + e + '/\n')


if __name__ == "__main__":
    host_set = set()

    parser = argparse.ArgumentParser(
        description="Generate DNS Zone files from Hosts files"
    )
    parser.add_argument(
        "files",
        metavar="HOSTS-FILES",
        help="List of hosts files to process into a DNS Zone File",
        nargs="+",
    )
    parser.add_argument("-u", "--unbound", action = "store_true", default=False)
    parser.add_argument("-d", "--dnsmasq", action = "store_true", default=False)
    parser.add_argument(
        "-o", "--output-directory", metavar="output", default="."
    )
    parser.add_argument(
        "-w", "--whitelist", metavar="whitelist", default="whitelist.txt"
    )
    parser.add_argument("-s", "--stats-file", default=None)

    args = parser.parse_args()
    soft_error = False

    if Path(args.whitelist).is_file():
        hosts_file = open(args.whitelist, "rb")
    else:
        try:
            hosts_file = get_hosts_file(args.whitelist)
        except ValueError:
            print("[{}]: Unknown path. Ignoring".format(args.whitelist))
    whitelist_count, whitelist_set, errored = add_hosts(hosts_file)
    soft_error |= errored

    created_whitelist = gen_whitelist_set(whitelist_set)

    total_domains = 0
    for arg in args.files:
        if Path(arg).is_file():
            hosts_file = open(arg, "rb")
        else:
            try:
                hosts_file = get_hosts_file(arg)
            except ValueError:
                print("[{}]: Unknown path. Ignoring".format(arg))
                continue

        old_count = len(host_set)
        host_count, host_set, errored = add_hosts(hosts_file)
        print(
            "Consuming {}/{} unique hosts from {}".format(
                len(host_set) - old_count, host_count, arg
            )
        )
        total_domains += host_count
        soft_error |= errored

    blocklist = gen_blocklist(host_set, created_whitelist)
    print(
        "Reduced {} hosts to {} unbound local zones and dnsmasq servers.".format(
            len(host_set), len(blocklist)
        )
    )
    if args.unbound:
        print("Generating unbound blocklist... ", end = '')
        write_unbound_blocklist_file(blocklist,
                                 Path(args.output_directory).joinpath('unbound-blocklist.conf'))
        print("Done!")
    if args.dnsmasq:
        print("Generating dnsmasq blocklist... ", end = '')
        write_dnsmasq_blocklist_file(blocklist,
                                 Path(args.output_directory).joinpath('dnsmasq-blocklist.conf'))
        print("Done!")
    if not (args.unbound | args.dnsmasq):
        print("Not writing any file because no output specified. Please specify output.")

    if args.stats_file:
        print("Writing stats to stats.txt")
        from datetime import datetime
        date = datetime.today().strftime('%Y-%m-%d')
        with open(args.stats_file, "a") as stats_file:
            stats_file.write(f"{date},{total_domains},{len(host_set)},{len(blocklist)}\n")
