# DNS BlockList Generator

Generate DNS Zone Blocklists for use with unbound(8)

Unbound supports wildcard entries. This script filters out subdomains of
pre-existing domains in the list.

![Blocked Hosts Stats](https://gitlab.com/api/v4/projects/33264311/jobs/artifacts/main/raw/stats.png?job=update-stats){width=55%}

## Usage

```
./gen-unbound-hosts.py [hosts-files...]
```

`hosts-file` can be either a local file or a URL. It will aggregate the hosts
from all input files and create one final deduplicated output DNS Zone file
for use with unbound.

The blocklists are generated once per day and the latest one can always be
found at the following link:

|                  | Unbound                                                                                                                                   | Dnsmasq                                                                                                                                   |
|------------------|-------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| StevenBlack List | [Block List](https://gitlab.com/api/v4/projects/33264311/jobs/artifacts/main/raw/blocklist-unbound-stevenblack.conf?job=unbound)  | [Block List](https://gitlab.com/api/v4/projects/33264311/jobs/artifacts/main/raw/blocklist-dnsmasq-stevenblack.conf?job=unbound)  |
| Consolidated     | [Block List](https://gitlab.com/api/v4/projects/33264311/jobs/artifacts/main/raw/blocklist-unbound-consolidated.conf?job=unbound) | [Block List](https://gitlab.com/api/v4/projects/33264311/jobs/artifacts/main/raw/blocklist-dnsmasq-consolidated.conf?job=unbound) |

The Consolidated List uses the StevenBlack List and a bunch of other sources.
Primarily, it relies on the list of lists curated by Wally3K under the
Advertising and Privacy sections.

## License

GPLv3+
